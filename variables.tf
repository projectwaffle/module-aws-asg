## Global variables

variable "resource_identities" {
  description = "Resource Identity according to PW Arhitecture Name Convention"
  type        = "map"
}

variable "global_tags" {
  description = "Global Tags"
  type        = "map"
}

## Module Specific variables

variable "module_tags" {
  description = "Module Tags"
  default     = {}
}

variable "name" {
  description = "Name for all resources to start with"
}

## Resource variables

# Key Pair

variable "key_name" {
  description = "Existing AWS SSH key Name used to connect to Instance"
  default     = ""
}

variable "public_key" {
  description = "The public key material"
  default     = ""
}

variable "create_key_pair" {
  description = "Create Key Pair by providing public key of own private one"
  default     = "false"
}

# Launch Template Insatance

variable "instance_os" {
  description = "Instance OS Type. Can be linux or windows"
  default     = "linux"
}

variable "enable_encryption" {
  description = "Enables EBS Encryption Defaults to false. Setting this to true will create KMS key and encrypt EBS with it"
  default     = ""
}

variable "root_block_device_ebs_delete_on_termination" {
  description = "Whether the volume should be destroyed on instance termination"
  default     = "true"
}

variable "root_block_device_name" {
  description = "The name of the root device to mount"
  default     = "/dev/xvda"
}

variable "root_block_device_ebs_volume_size_defaults" {
  description = "The size of the root volume in gigabytes"

  default = {
    linux   = "8"
    windows = "32"
  }
}

variable "root_block_device_ebs_volume_size" {
  description = "The size of the root volume in gigabytes"
  default     = ""
}

variable "root_block_device_ebs_volume_type" {
  description = "The type of root volume. Can be standard, gp2, or io1"
  default     = "gp2"
}

variable "root_block_device_ebs_iops" {
  description = "The amount of provisioned IOPS for root device. This must be set with a volume_type of io1"
  default     = "250"
}

variable "root_block_device_ebs_snapshot_id" {
  description = "The Snapshot ID to mount root device block"
  default     = ""
}

variable "iam_instance_profile_arn" {
  description = "The IAM Instance Profile ARN to launch the instance with"
  default     = ""
}

variable "cpu_credits" {
  description = "The credit option for CPU usage. Can be standard or unlimited"
  default     = "standard"
}

variable "security_group_id" {
  description = "List of security groups"
  type        = "list"
}

variable "instance_type" {
  description = "The type of the instance"
  default     = "t3.nano"
}

variable "instance_initiated_shutdown_behavior" {
  description = "Shutdown behavior for the instance. Can be stop or terminate"
  default     = "stop"
}

variable "create_custom_userdata" {
  description = "Create custome Userdata script"
  default     = "false"
}

variable "custom_userdata_script_path" {
  description = "Custom Userdata script path"
  default     = ""
}

variable "custom_userdata_script_content" {
  description = "Custom Userdata script Content, might be received via data template"
  default     = ""
}

# Elastic IP

variable "create_eip" {
  description = "Set to false if you do not want to create EIP EC2 Instance for module"
  default     = "false"
}

variable "eip_tags" {
  description = "Custome Elastic IP Tags"
  default     = {}
}

# KMS Key Encryption

variable "kms_key_id" {
  description = "KMS Key ID used for encryption of the logs. works with `var.enable_encryption`"
  default     = ""
}

# ASG 

variable "create_asg" {
  description = "Set to false if you do not want to create ASG for module"
  default     = "true"
}

variable "instance_name_prefix" {
  description = "Instance Name Prefix for Tagging the Name. It will add count inxed at the end plus 1"
  default     = ""
}

variable "max_size" {
  description = "The maximum size of the autoscale group"
  default     = "5"
}

variable "min_size" {
  description = "The minimum size of the autoscale group"
  default     = "1"
}

variable "target_group_arns" {
  description = "A list of aws_alb_target_group ARNs, for use with Application Load Balancing"
  default     = []
}

variable "default_cooldown" {
  description = "The amount of time, in seconds, after a scaling activity completes before another scaling activity can start"
  default     = 300
}

variable "health_check_grace_period" {
  description = "Time (in seconds) after instance comes into service before checking health"
  default     = 300
}

variable "health_check_type" {
  description = "Controls how health checking is done. Valid values are `EC2` or `ELB`"
  default     = "EC2"
}

variable "force_delete" {
  description = "Allows deleting the autoscaling group without waiting for all instances in the pool to terminate. You can force an autoscaling group to delete even if it's in the process of scaling a resource. Normally, Terraform drains all the instances before deleting the group. This bypasses that behavior and potentially leaves resources dangling"
  default     = false
}

variable "enabled_metrics" {
  description = "A list of metrics to collect. The allowed values are `GroupMinSize`, `GroupMaxSize`, `GroupDesiredCapacity`, `GroupInServiceInstances`, `GroupPendingInstances`, `GroupStandbyInstances`, `GroupTerminatingInstances`, `GroupTotalInstances`"
  type        = "list"

  default = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]
}

variable "termination_policies" {
  description = "A list of policies to decide how the instances in the auto scale group should be terminated. The allowed values are `OldestInstance`, `NewestInstance`, `OldestLaunchConfiguration`, `ClosestToNextInstanceHour`, `Default`"
  default     = ["Default"]
}

variable "suspended_processes" {
  description = "A list of processes to suspend for the AutoScaling Group. The allowed values are `Launch`, `Terminate`, `HealthCheck`, `ReplaceUnhealthy`, `AZRebalance`, `AlarmNotification`, `ScheduledActions`, `AddToLoadBalancer`. Note that if you suspend either the `Launch` or `Terminate` process types, it can prevent your autoscaling group from functioning properly."
  default     = []
}

variable "subnet_id" {
  description = "List of subnet IDs"
  type        = "list"
}

variable "desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the group."
  default     = "1"
}

variable "placement_group" {
  description = "The name of the placement group into which you'll launch your instances, if any"
  default     = ""
}

variable "asg_tags" {
  description = "Custom ASG Tags"
  default     = {}
}
