# Locals

locals {
  kms_key_id                        = "${element(concat(list(var.kms_key_id), list("")), 0)}"
  root_block_device_ebs_iops        = "${(var.root_block_device_ebs_volume_type == "io1") && (var.root_block_device_ebs_iops != 0) ? var.root_block_device_ebs_iops : 0}"
  enable_encryption                 = "${var.enable_encryption == "" ? false : true}"
  root_block_device_ebs_snapshot_id = "${!local.enable_encryption ? var.root_block_device_ebs_snapshot_id : element(list(""), 0)}"
  instance_ids                      = "${concat(aws_instance.main.*.id, list(""))}"
  iam_instance_profile_arn          = "${element(concat(list(var.iam_instance_profile_arn), list("")), 0)}"
  image_id                          = "${lookup(map("linux", data.aws_ami.linux.id, "windows", data.aws_ami.windows.id), var.instance_os, data.aws_ami.linux.id)}"
  root_block_device_ebs_volume_size = "${var.root_block_device_ebs_volume_size != "" ? var.root_block_device_ebs_volume_size : lookup(var.root_block_device_ebs_volume_size_defaults, var.instance_os, 8)}"
  launch_template                   = "${element(concat(aws_launch_template.main.*.id, list("")), 0)}"
  placement_group                   = "${element(concat(list(var.placement_group), list("")), 0)}"
  instance_name_prefix              = "${lower(var.instance_name_prefix != "" ? var.instance_name_prefix : var.name)}"
  tags_key                          = "${keys(merge(map("Name", format("%s", local.instance_name_prefix)), var.global_tags, var.module_tags, var.asg_tags))}"
  tags_values                       = "${values(merge(map("Name", format("%s", local.instance_name_prefix)), var.global_tags, var.module_tags, var.asg_tags))}"
  tags                              = "${concat(null_resource.tags.*.triggers)}"
  key_name                          = "${element(concat(aws_key_pair.main.*.key_name, list(var.key_name), list("")), 0)}"
  template_cloudinit_config_main    = "${concat(data.template_cloudinit_config.main.*.rendered, list(""))}"
  template_cloudinit_config_custom  = "${base64encode(element(concat(data.template_file.custom_userdata.*.rendered, list(var.custom_userdata_script_content), list("")), 0))}"
}

resource "null_resource" "tags" {
  count = "${length(local.tags_key)}"

  triggers = "${map("key" , element(local.tags_key, count.index), "value" , element(local.tags_values, count.index), "propagate_at_launch" , "true")}"
}

resource "aws_key_pair" "main" {
  count = "${var.create_asg && var.create_key_pair ? 1 : 0 }"

  key_name   = "${format("%s-%s", var.name, var.resource_identities["key_name"],)}"
  public_key = "${var.public_key}"
}

# Resource Elastic IP

resource "aws_eip" "main" {
  count = "${var.create_asg && var.create_eip ? length(local.instance_ids) : 0}"

  vpc      = true
  instance = "${local.instance_ids}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["eip"],)), var.global_tags, var.module_tags, var.eip_tags)}"
}

# Resource UserData input

data "template_file" "main_userdata" {
  count = "${var.create_asg ? 1 : 0 }"

  template = "${file("${path.module}/templates/${var.instance_os}_init.tpl")}"

  vars {
    hostname = "${format("%s%d", local.instance_name_prefix, (count.index + 1))}"
  }
}

data "template_cloudinit_config" "main" {
  count = "${var.create_asg ? 1 : 0 }"

  gzip          = "${var.instance_os == "windows" ? false : true }"
  base64_encode = true

  part {
    filename     = "sethostname.script"
    content_type = "text/x-shellscript"
    content      = "${element(data.template_file.main_userdata.*.rendered, count.index)}"
  }
}

data "template_file" "custom_userdata" {
  count = "${var.create_asg && var.create_custom_userdata && var.custom_userdata_script_path != "" ? 1 : 0 }"

  template = "${file("${var.custom_userdata_script_path}")}"
}

# Resource Launch Template

resource "aws_launch_template" "main" {
  count = "${var.create_asg ? 1 : 0}"

  name_prefix = "${format("%s-%s", var.name, var.resource_identities["launch_template"])}"
  description = "${format("%s-%s", var.name, var.resource_identities["launch_template"])}"

  block_device_mappings {
    device_name = "${var.root_block_device_name}"

    ebs {
      delete_on_termination = "${var.root_block_device_ebs_delete_on_termination}"
      encrypted             = "${var.enable_encryption}"
      iops                  = "${local.root_block_device_ebs_iops}"
      kms_key_id            = "${local.kms_key_id}"
      volume_size           = "${local.root_block_device_ebs_volume_size}"
      volume_type           = "${var.root_block_device_ebs_volume_type}"
      snapshot_id           = "${local.root_block_device_ebs_snapshot_id}"
    }
  }

  credit_specification {
    cpu_credits = "${var.cpu_credits}"
  }

  instance_initiated_shutdown_behavior = "${var.instance_initiated_shutdown_behavior}"
  key_name                             = "${local.key_name}"
  instance_type                        = "${var.instance_type}"

  iam_instance_profile {
    arn = "${local.iam_instance_profile_arn}"
  }

  image_id = "${local.image_id}"

  network_interfaces {
    associate_public_ip_address = "true"
    security_groups             = ["${var.security_group_id}"]
    delete_on_termination       = "true"
  }

  user_data = "${var.create_custom_userdata ? local.template_cloudinit_config_custom : element(local.template_cloudinit_config_main, count.index)}"
}

# AutoScaling Group

resource "aws_autoscaling_group" "main" {
  count = "${var.create_asg ? 1 : 0}"

  name_prefix               = "${format("%s-%s", var.name, var.resource_identities["asg"])}"
  max_size                  = "${var.max_size}"
  min_size                  = "${var.min_size}"
  default_cooldown          = "${var.default_cooldown}"
  health_check_grace_period = "${var.health_check_grace_period}"
  health_check_type         = "${var.health_check_type}"
  desired_capacity          = "${var.desired_capacity}"
  force_delete              = "${var.force_delete}"
  placement_group           = "${local.placement_group}"
  vpc_zone_identifier       = ["${var.subnet_id}"]
  target_group_arns         = ["${var.target_group_arns}"]
  termination_policies      = ["${var.termination_policies}"]
  suspended_processes       = ["${var.suspended_processes}"]

  launch_template {
    id      = "${local.launch_template}"
    version = "$$Latest"
  }

  timeouts {
    delete = "5m"
  }

  tags = ["${local.tags}"]
}
