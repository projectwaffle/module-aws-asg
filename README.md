# AWS ASG with launch Template Terraform module

[![CircleCI](https://circleci.com/bb/projectwaffle/module-aws-asg.svg?style=svg)](https://circleci.com/bb/projectwaffle/module-aws-asg/)

Terraform module which creates EC2 Instace and related resources on AWS. Might be used for different modules

These types of resources are supported:

* [AutoScaling Group](https://www.terraform.io/docs/providers/aws/r/autoscaling_group.html)
* [Launch Temaplate](https://www.terraform.io/docs/providers/aws/r/launch_template.html)
* [EBS Volume](https://www.terraform.io/docs/providers/aws/r/ebs_volume.html)
* [Elastic IP](https://www.terraform.io/docs/providers/aws/r/eip.html)

Sponsored by [Draw.io - the best way to draw AWS diagrams](https://www.draw.io/)

## Usage

```hcl
module "asg" {
  source = "bitbucket.org/projectwaffle/module-aws-asg.git?ref=tags/v0.0.1"

  name = "training-prod"

  resource_identities = {
    eip             = "asg-eip"
    launch_template = "asg-lt"
    asg             = "asg"
  }

  global_tags = "${var.global_tags}"

  security_group_id        = ["${module.sg_asg.security_group_id}"]
  subnet_id                = "${module.vpc.public_subnets}"
  iam_instance_profile_arn = "${module.asg_assume_role.assume_role_instance_profile_arn}"

  min_size = "0"
  desired_capacity = "0"
}
```

Security group, subnet ID and Subnet AZ are imported from different modules. Instance profile as well. Can be specified just IDs

## Custom Userdata and Instance hostname

By default, module will create userdata to setup next default settings:

 * Set up hostname - useing variable `instance_name_prefix = "app-p"`
 * Do update to all software (amazon linux only)

There is an option to add custom userdata script with next:
```hcl
module "asg" {

  create_custom_userdata      = "true"
  custom_userdata_script_path = "./custom_script.sh"
}
```

Just point to custome script file.

## Resource Identity

In order to add custom names to resources, each module has two variables to be used for this.
* Variable called `name` - this will be used as the base name for each resource in the module
    * `name = "training-prod"`
* MAP variable called `resource_identities` - used to append specific name to each resoruce
```hcl
  resource_identities = {
    config_config_rule            = "config-rule"
    config_configuration_recorder = "config-recorder"
  }
```

Both variables should be declared in module section:
```hcl
module "config" {
  ......
  name = "training-prod"
  resource_identities = {
    eip             = "asg-eip"
    launch_template = "asg-lt"
    asg             = "asg"
  }
  ......
}
```

Next is an example of variable used in the Module resoruce names or tgas 'Name':
```hcl
  name  = "${format("%s-%s", var.name, var.resource_identities["instance"])}"
  tags  = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["instance"],)), var.global_tags, var.module_tags, var.instance_tags)}"
```

## Resource Tagging

There are three level of resource tagging declaration. Global Tagging mandatory for any module. Place this variable in root folder

* Global level - declare tagging strategy that will be applied on all the AWS resources

```hcl
variable "global_tags" {
  type = "map"

  default = {
    environment     = "prod"
    role            = "infrastructure"
    version         = "0.1"
    owner           = "Sergiu Plotnicu"
    bu              = "IT"
    customer        = "project waffle"
    project         = "training"
    confidentiality = "open"
    compliance      = "N/A"
    encryption      = "disabled"
  }
}
```

* Module level - taggig that will affect only resources created by the module itself
```hcl
module "instance" {

  module_tags = {
    bu = "security"
  }
  global_tags         = "${var.global_tags}"
}
```

* Resource level - each resource can be tagged with custome tag or tage that will rewrite all the others one

```hcl
variable "instance_tags" {
  description = "Custome Instance Tags"
  default     = {
    role = "app"
  }
}
```

Combination of all this variables should cover all the tagging requirements.
Tags are beeing merged so, resource level ones, will rewrite modules one, that will rewrite global ones.

## Key Pair management

EC2 instances need keypair to connect to SSH (linux) or generate Administrator password (Window system).
There are couple of ways to manage Keypair

* Variable called `key_name` - to use existing AWS Console or previously created Keypair
    * `key_name = "key_name = "training-keypair-ireland""`

* Create new Keypair. `public_key` required if `create_key_pair` is `true`

```hcl
module "instance" {

  create_key_pair = "true"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohBK41 sergiu.plotnicu@project-waffle.com"
}  
```  

* No Keypair at all - default Option. This option is useful when Instances are used in a imutable way either use of SSM agent to initiate session on instance. Details can be found here - [AWS Systems Manager Session Manager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html)



## Terraform version

Terraform version 0.11.10 or newer is required for this module to work.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| asg\_tags | Custom ASG Tags | map | `<map>` | no |
| cpu\_credits | The credit option for CPU usage. Can be standard or unlimited | string | `standard` | no |
| create\_asg | Set to false if you do not want to create ASG for module | string | `true` | no |
| create\_custom\_userdata | Create custome Userdata script | string | `false` | no |
| create\_eip | Set to false if you do not want to create EIP EC2 Instance for module | string | `false` | no |
| create\_key\_pair | Create Key Pair by providing public key of own private one | string | `false` | no |
| custom\_userdata\_script\_content | Custom Userdata script Content, might be received via data template | string | `` | no |
| custom\_userdata\_script\_path | Custom Userdata script path | string | `` | no |
| default\_cooldown | The amount of time, in seconds, after a scaling activity completes before another scaling activity can start | string | `300` | no |
| desired\_capacity | The number of Amazon EC2 instances that should be running in the group. | string | `1` | no |
| eip\_tags | Custome Elastic IP Tags | map | `<map>` | no |
| enable\_encryption | Enables EBS Encryption Defaults to false. Setting this to true will create KMS key and encrypt EBS with it | string | `` | no |
| enabled\_metrics | A list of metrics to collect. The allowed values are `GroupMinSize`, `GroupMaxSize`, `GroupDesiredCapacity`, `GroupInServiceInstances`, `GroupPendingInstances`, `GroupStandbyInstances`, `GroupTerminatingInstances`, `GroupTotalInstances` | list | `<list>` | no |
| force\_delete | Allows deleting the autoscaling group without waiting for all instances in the pool to terminate. You can force an autoscaling group to delete even if it's in the process of scaling a resource. Normally, Terraform drains all the instances before deleting the group. This bypasses that behavior and potentially leaves resources dangling | string | `false` | no |
| global\_tags | Global Tags | map | - | yes |
| health\_check\_grace\_period | Time (in seconds) after instance comes into service before checking health | string | `300` | no |
| health\_check\_type | Controls how health checking is done. Valid values are `EC2` or `ELB` | string | `EC2` | no |
| iam\_instance\_profile\_arn | The IAM Instance Profile ARN to launch the instance with | string | `` | no |
| instance\_initiated\_shutdown\_behavior | Shutdown behavior for the instance. Can be stop or terminate | string | `stop` | no |
| instance\_name\_prefix | Instance Name Prefix for Tagging the Name. It will add count inxed at the end plus 1 | string | `` | no |
| instance\_os | Instance OS Type. Can be linux or windows | string | `linux` | no |
| instance\_type | The type of the instance | string | `t3.nano` | no |
| key\_name | Existing AWS SSH key Name used to connect to Instance | string | `` | no |
| kms\_key\_id | KMS Key ID used for encryption of the logs. works with `var.enable_encryption` | string | `` | no |
| max\_size | The maximum size of the autoscale group | string | `5` | no |
| min\_size | The minimum size of the autoscale group | string | `1` | no |
| module\_tags | Module Tags | map | `<map>` | no |
| name | Name for all resources to start with | string | - | yes |
| placement\_group | The name of the placement group into which you'll launch your instances, if any | string | `` | no |
| public\_key | The public key material | string | `` | no |
| resource\_identities | Resource Identity according to PW Arhitecture Name Convention | map | - | yes |
| root\_block\_device\_ebs\_delete\_on\_termination | Whether the volume should be destroyed on instance termination | string | `true` | no |
| root\_block\_device\_ebs\_iops | The amount of provisioned IOPS for root device. This must be set with a volume_type of io1 | string | `250` | no |
| root\_block\_device\_ebs\_snapshot\_id | The Snapshot ID to mount root device block | string | `` | no |
| root\_block\_device\_ebs\_volume\_size | The size of the root volume in gigabytes | string | `` | no |
| root\_block\_device\_ebs\_volume\_size\_defaults | The size of the root volume in gigabytes | map | `<map>` | no |
| root\_block\_device\_ebs\_volume\_type | The type of root volume. Can be standard, gp2, or io1 | string | `gp2` | no |
| root\_block\_device\_name | The name of the root device to mount | string | `/dev/xvda` | no |
| security\_group\_id | List of security groups | list | - | yes |
| subnet\_id | List of subnet IDs | list | - | yes |
| suspended\_processes | A list of processes to suspend for the AutoScaling Group. The allowed values are `Launch`, `Terminate`, `HealthCheck`, `ReplaceUnhealthy`, `AZRebalance`, `AlarmNotification`, `ScheduledActions`, `AddToLoadBalancer`. Note that if you suspend either the `Launch` or `Terminate` process types, it can prevent your autoscaling group from functioning properly. | list | `<list>` | no |
| target\_group\_arns | A list of aws_alb_target_group ARNs, for use with Application Load Balancing | list | `<list>` | no |
| termination\_policies | A list of policies to decide how the instances in the auto scale group should be terminated. The allowed values are `OldestInstance`, `NewestInstance`, `OldestLaunchConfiguration`, `ClosestToNextInstanceHour`, `Default` | list | `<list>` | no |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Authors

Module is maintained by [Sergiu Plotnicu](https://bitbucket.org/projectwaffle/)

## License

Licensed under Sergiu Plotnicu, please contact him at - sergiu.plotnicu@gmail.com


